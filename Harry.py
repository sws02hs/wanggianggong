'''
import numpy as np
import matplotlib.pyplot as plt
import math
nx=40
dx = 1/nx
u = 1 
cfl = 0.1
dt=cfl*dx/u
nt = int(0.125/dt)+1 # number of time steps
space=np.linspace(0,1,nx)
time=np.linspace(0,0.125,nt)
def initial_condition(x=[]):
    there are three initial condition a is sinx,b is 0.5*(1-4*cosx)in period 
    from 0 to 0.5 and 0 in period from 0.5 to 1,c is 1 form 0 to 0.5 and 0 
    from 0.5 to 1
    a=np.sin(x)
    b=[]
    for i in x:
        if i<0.5:
            b.append(0.5*(1-np.cos(4*math.pi*i)))
        else:
            b.append(0)
    c=[]
    for j in x:
        if j<0.5:
            c.append(1)
        else:
            c.append(0)
    return a,b,c
#u = np.ones(nx)
#u[int(0.5/dx):int(1/dx + 1)] = 2 #create initial wave
u=initial_condition(space)[1]
# plot initial wave
plt.plot(space,u, 'r',lw=3, label = 'init')
for n in range(nt):
    un = u.copy() #update initial condition
    for i in range(1,nx):
        u[i] = un[i] - cfl * dt / dx * (un[i] - un[i-1])#FTBS
    u[0]=u[nx-1]
    plt.plot(space, u ,'b',lw =0.2,label= 'FTBS')
    plt.xlabel('space')
    plt.ylabel('velocity')
'''
    




import numpy.linalg as la
import matplotlib.pyplot as plt
import numpy as np
import math

def FTBS(phiOld, c, nt):
    nx = len(phiOld)
    phi = phiOld.copy()
    x=np.linspace(0,1,nx)
    # FTCS for all time steps
    for it in range(nt):
        # loop over all internal points
        for i in range(1,nx):
            phi[i] = phiOld[i] \
                   -c*(phiOld[i]-phiOld[i-1])
        #periodic boundary condition
        phi[0] = phi[-1]
        # Update phi for next time-step and plot
        phiOld=phi.copy()
    plt.plot(x, phi ,'b',lw =0.2,label= 'FTBS')
    plt.show()
    return phi

def BTCS(phiold, c, nt):
    nx = len(phiold)
    # array representing BTCS
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    x=np.linspace(0,1,nx)
    #periodic boundary condition
    M[0,0] = 1.
    M[-1,-1] = 1.
    for i in range(1,nx-1):
        M[i,i-1] = -c/2
        M[i,i] = 1
        M[i,i+1] = c/2
    # BTCS for all time steps
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(M,phiold)
        #plt.plot(x, phi ,'black',lw =0.2,label= 'FTBS')
        phiold=phi.copy()
    return phi

def BTBS(phiold, c, nt):
    nx = len(phiold)
    # array representing BTBS
    M = np.zeros([nx,nx])
    phi = phiold.copy()
    x=np.linspace(0,1,nx)
    # periodic boundary condition
    M[0,0] = 1+c
    M[0,-1] = -c
    for i in range(1,nx):
        M[i,i-1] = -c
        M[i,i] = 1+c
#        M[i,i+1] = 0
    # BTBS for all time steps
    for it in range(nt):
        # RHS for periodic boundary conditions
        phi = la.solve(M,phiold)
        phiold=phi.copy()
    plt.plot(x, phi ,'b',lw =0.2,label= 'BTBS')
    plt.show()
    return phi

def initial_condition(x=[]):
    '''there are three initial condition a is sinx,b is 0.5*(1-4*cosx)in period 
    from 0 to 0.5 and 0 in period from 0.5 to 1,c is 1 form 0 to 0.5 and 0 
    from 0.5 to 1'''
    a=np.sin(x)
    b=[]
    for i in x:
        if i<0.5:
            b.append(0.5*(1-np.cos(4*math.pi*i)))
        else:
            b.append(0)
    c=[]
    for j in x:
        if j<0.5:
            c.append(1)
        else:
            c.append(0)
    return a,b,c

def analytic1(x,t,u):
    Analytic=[]
    for i in t:
        Analytic=[]
        for j in x:
            if j<0.5:
                Analytic.append(0.5*(1-np.cos(4*math.pi*(j-u*i))))
            else:
                Analytic.append(0)
        Analytic[0]=Analytic[-1]
        plt.plot(x,Analytic,'r',lw=0.2)
    return Analytic

def analytic2(x,t,u):
    Analytic=[]
    for i in t:
        Analytic=[]
        for j in x:
            if j<0.5:
                Analytic.append(1)
            else:
                Analytic.append(0)
        Analytic[0]=Analytic[-1]
        plt.plot(x,Analytic,'green',lw=0.2)    
    return Analytic

def L2ErrorNorm(phi, phiExact):
    """Calculates the L2 error norm (RMS error) of phi in comparison to
    phiExact, ignoring the boundaries"""
    #remove one of the end points
    phi = phi[1:-1]
    phiExact = phiExact[1:-1]
    # calculate the error and the error norms
    phiError = phi - phiExact
    L2 = np.sqrt(sum(phiError**2)/sum(phiExact**2))
    return L2

nx=41
dx = 1/(nx-1)
u = 1 
cfl = 0.1
dt=cfl*dx/u
print(dt)
totol_T=1.0
nt = int((totol_T+1e-12)/dt) # number of time steps
space=np.linspace(0,1,nx)
time=np.linspace(0,totol_T,nt)
phiold=initial_condition(space)
nRevolutions = dt*nt*u
print('Running for ', dt*nt, ' time. ', nRevolutions, ' revolutions')
FTBS_1=FTBS(phiold[1], cfl, nt)
BTBS_1=BTBS(phiold[1],cfl,nt)
BTCS_1=BTCS(phiold[1],cfl,nt)
#Exact_1=analytic1(space,time,u)
Exact_1 = initial_condition((space - u*nt*dt)%1.)[1]

plt.plot(space, FTBS_1, label='FTBS')
plt.plot(space, BTBS_1, label='BTBS')
plt.plot(space, Exact_1, label='exact')
plt.legend()
plt.show()

#Exact_2=analytic2(space,time,u)
#FTBS_2=FTBS(phiold[2], cfl, nt)
#BTBS_2=BTBS(phiold[2],cfl,nt)
#BTCS_2=BTCS(phiold[2],cfl,nt)
BTBS1_error = L2ErrorNorm(np.array(BTBS_1),np.array(Exact_1))
#L2ErrorNorm(np.array(BTBS_2),np.array(Exact_2))
#L2ErrorNorm(np.array(BTCS_1),np.array(Exact_1))
#L2ErrorNorm(np.array(BTCS_2),np.array(Exact_2))
FTBS1_error = L2ErrorNorm(np.array(FTBS_1),np.array(Exact_1))
#L2ErrorNorm(np.array(FTBS_2),np.array(Exact_2))

print('BTBS1_error = ', BTBS1_error)
print('FTBS1_error = ', FTBS1_error)

